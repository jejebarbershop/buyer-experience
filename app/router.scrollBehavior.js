/* eslint-disable */
export default function (to, from, savedPosition, ozzy) {
  if (savedPosition) {
    return savedPosition;
  }

  if (to.hash) {
    return new Promise((resolve, reject) => {
      // This is wrapped by a setTimeout because otherwise some components that take time to render content (such as those that render markdown)
      // fail to scroll because of the timing (the component that vue needs to scroll to has not been inserted into the DOM)
      setTimeout(() => {
        const element = document.querySelector(to.hash);
        const navBarOffset = 160;

        const bodyRect = document.body.getBoundingClientRect();
        const elemRect = element.getBoundingClientRect();
        const offset = elemRect.top - bodyRect.top;

        // The parent of the element must have css "position:relative"
        if ('scrollBehavior' in document.documentElement.style) {
          resolve(
            window.scrollTo({
              top: offset - navBarOffset,
              behavior: 'smooth',
            }),
          );
        } else {
          resolve(window.scrollTo(0, offset - navBarOffset));
        }
      }, 50);
    });
  }
}
